﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace VDelStr
{
    internal class Program
    {
        //ReadMe
        //create for every file folder with same name before @
        //in text file split lines with ;
        private static void Main()
        {
            #region Variables
            var pathLocation = Directory.GetCurrentDirectory() + "\\paths.txt";
            var sourceLocations = File.ReadAllLines(pathLocation).ToList();
            var sourcePath = sourceLocations[0];
            var destinationPath = sourceLocations[1];
            var sourceDir = new DirectoryInfo(sourcePath);
            var destinationDir = new DirectoryInfo(destinationPath);
            List<string> lstNamesBeforeDog = new List<string>();
            List<string> lstFileInSource = new List<string>();
            #endregion

            #region Operations
            CheckDirectory(sourcePath);
            CheckDirectory(destinationPath); // check for existing destination directory
            GetNamesBeforeDog(ref lstNamesBeforeDog, sourceDir);
            GetListFilesInSourceAndDestDirectory(ref lstFileInSource, sourceDir);
            CheckManyFolders(destinationPath, lstNamesBeforeDog);
            #endregion

            #region Cycles
            for (int i = 0; i < lstFileInSource.Count; i++)
            {
                    var sourceSkip1File = File.ReadLines(sourcePath + "\\" + lstFileInSource[i]).Skip(1);
                    File.WriteAllLines(destinationPath + "\\" + lstNamesBeforeDog[i] + "\\members", sourceSkip1File);
            }

            foreach (var file in destinationDir.GetFiles())
            {
                var destinationStartFile = File.ReadAllLines(file.FullName);
                FindReplaceSpaces(ref destinationStartFile); // знаходимо і видал усі пробели і заміняємо на ";"
            }
            #endregion

            Console.WriteLine("Success!");
            Console.ReadKey();
        }

        private static void CheckDirectory(string path) // create directory if not exist
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            else
                Console.WriteLine($"Директорiя: \"{path}\", уже iснує");
        }

        private static void CheckFolder(string destinationPath, string folderName) // create folder if not exist
        {
            string pathFullDirectory = destinationPath + folderName;

            if (!Directory.Exists(pathFullDirectory))
                Directory.CreateDirectory(pathFullDirectory);
            else
                Console.WriteLine($"Папка: \"{folderName}\" уже створена");
        }

        private static void CheckManyFolders(string destinationPath, List<string> folderName) // check multiple folders. Create/replaced new if not exist
        {
            List<string> nonExistantFolders = new List<string>();

            for (int i = 0; i < folderName.Count - 1; i++)
            {
                string pathFullDirectory = destinationPath + "\\" + folderName[i];

                if (!Directory.Exists(pathFullDirectory))
                {
                    Directory.CreateDirectory(pathFullDirectory);
                    nonExistantFolders.Add(folderName[i]);
                }
            }
            if (nonExistantFolders.Count != 0)
            {
                foreach (var folder in nonExistantFolders)
                {
                    Console.WriteLine($"Folder {folder} was created/replaced.");
                }
            }
            else
            {
                Console.WriteLine("Non file was created");
            }
        }

        private static void GetListFilesInSourceAndDestDirectory(ref List<string> lstFileInSource, DirectoryInfo sourceDir)
        {
            Log log = new Log();
            foreach (var file in sourceDir.GetFiles())
            {
                lstFileInSource.Add(file.Name);
                if (!CheckUtf(file)) //перев кодування файлу
                    log.Add(file);
            }
        }

        private static void GetNamesBeforeDog(ref List<string> lstNamesBeforeDog, DirectoryInfo sourceDir) // get names before @
        {
            foreach (var item in sourceDir.GetFiles())
            {
                var nameBeforeDog = item.Name.Substring(0, item.Name.IndexOf('@'));
                lstNamesBeforeDog.Add(nameBeforeDog);
            }

        }

        private static void FindReplaceSpaces(ref string[] liness)
        {
            for (var i = 0; i < liness.Length; i++)
            {
                for (var j = 1; j < liness[i].Length; j++)
                {
                    liness[i] = (liness[i].Trim() + ";").Trim();
                }
            }
        }

        private static bool CheckUtf(FileSystemInfo item) // check encoding for UTF-8
        {
            var strReader = new StreamReader(item.FullName);
            var encoding = strReader.CurrentEncoding;
            return encoding == System.Text.Encoding.UTF8;
        }
    }
}