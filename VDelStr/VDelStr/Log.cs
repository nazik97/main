﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace VDelStr
{
    internal class Log
    {
        private readonly string _pathLogLocation = Directory.GetCurrentDirectory() + "\\logs.txt";

        public void Add(FileInfo item)
        {
            using (var sWr = new StreamWriter(_pathLogLocation, true, Encoding.UTF8))
            {
                sWr.WriteLine("Файл: {0}\n час та дата: {1}\n", item.Name, DateTime.Now);
            }
        }
    }
}